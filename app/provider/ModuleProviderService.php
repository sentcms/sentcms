<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
declare (strict_types = 1);
namespace app\provider;

use think\Service;
use app\services\module\ModuleService;
use think\Db;

/**
 * 模型服务类
 */
class ModuleProviderService extends Service{
	public function register(){
		// 服务注册
	}

	public function boot(){
		// 服务启动
	}
}
