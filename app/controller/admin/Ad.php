<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\controller\admin;

use app\services\Adservice;

/**
 * @title 广告管理
 * @description 广告管理
 */
class Ad extends Base {

	/**
	 * @title 广告位管理
	 */
	public function index(Adservice $service) {
		$list = $service->getAdPlaceList($this->request);

		$this->data = array(
			'list' => $list,
			'page' => $list->render(),
		);
		return $this->fetch();
	}

	/**
	 * @title 广告位添加
	 */
	public function add(Adservice $service) {
		if ($this->request->isPost()) {
			try {
				$service->createAdPlace($this->request);
				return $this->success("添加成功！", url('/admin/ad/index'));
			} catch (\think\Exception $e) {
				return $this->error($e->getMessage());
			}
		} else {
			$this->data = array(
				'keyList' => \app\model\ads\AdPlace::$keyList,
			);
			return $this->fetch('admin/public/edit');
		}
	}

	/**
	 * @title 广告位编辑
	 */
	public function edit(Adservice $service) {
		if ($this->request->isPost()) {
			try {
				$service->updateAdPlace($this->request);
				return $this->success("修改成功！", url('/admin/ad/index'));
			} catch (\think\Exception $e) {
				return $this->error($e->getMessage());
			}
		} else {
			$this->data = array(
				'info' => $service->getAdPlaceDetail($this->request),
				'keyList' => \app\model\ads\AdPlace::$keyList,
			);
			return $this->fetch('admin/public/edit');
		}
	}

	/**
	 * @title 广告位删除
	 */
	public function del() {
		$id = $this->getArrayParam('id');

		if (empty($id)) {
			return $this->error("非法操作！");
		}
		$map['id'] = array('IN', $id);
		$result = $this->adplace->where($map)->delete();
		if ($result) {
			return $this->success("删除成功！");
		} else {
			return $this->error("删除失败！");
		}
	}

	/**
	 * @title 广告列表
	 */
	public function lists(Adservice $service) {
		$list = $service->getAdByPlace($this->request);
		$this->data = array(
			'id' => $this->request->get('id', 0),
			'list' => $list,
			'page' => $list->render(),
		);
		return $this->fetch();
	}

	/**
	 * @title 添加广告
	 */
	public function addad($id) {
		if ($this->request->isPost()) {
			$data = $this->request->post();

			$result = AdModel::create($data);
			if ($result) {
				return $this->success("添加成功！", url('/admin/ad/lists', ['id' => $data['place_id']]));
			} else {
				return $this->error('添加失败！');
			}
		} else {
			$info['place_id'] = $id;
			$this->data = array(
				'info' => $info,
				'keyList' => \app\model\ads\Ad::$keyList,
			);
			return $this->fetch('admin/public/edit');
		}
	}

	/**
	 * @title 编辑广告
	 */
	public function editad($id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->post();

			$result = AdModel::update($data, ['id' => $data['id']]);
			if ($result) {
				return $this->success("修改成功！", url('/admin/ad/lists', ['id' => $data['place_id']]));
			} else {
				return $this->error('修改失败！');
			}
		} else {
			$info = AdModel::find($id);
			if (!$info) {
				return $this->error("非法操作！");
			}
			$this->data = array(
				'info' => $info,
				'keyList' => \app\model\ads\Ad::$keyList,
			);
			return $this->fetch('admin/public/edit');
		}
	}

	/**
	 * @title 删除广告
	 */
	public function delad() {
		$id = $this->getArrayParam('id');

		if (empty($id)) {
			return $this->error("非法操作！");
		}
		$map['id'] = array('IN', $id);
		$result = db('ad')->where($map)->delete();
		if ($result) {
			return $this->success("删除成功！");
		} else {
			return $this->error("删除失败！");
		}
	}
}